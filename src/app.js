import 'dotenv-flow/config';
import { UserRepository } from './repository/user_repository';
import { server } from './server';

let port = process.env.PORT || 3000;

server.listen(port, ()=>{
    console.log('server is running on port '+port);
})


process.on("SIGINT", () => {
    console.log("exiting…");
    process.exit(0);
});

process.on("exit", () => {
    console.log("exiting…");
    process.exit(0);
});