export class Categorie {

    catId
    title;
    subjects = [];

    constructor(catId, title) {
        this.catId = catId;
        this.title = title;
    }
}