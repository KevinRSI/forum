import {
    Router
} from "express";
import {
    UserRepository
} from "../repository/user_repository.js";
export const userRoute = Router();


userRoute.delete('/delete/:id', async (req, res) => {
    try {
        await new UserRepository().Delete(req.params.id);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({message: 'C est la loose'})
    }

})

userRoute.post('/add', async (req, res) => {
    try{
        await new UserRepository().Add(req.body);
        res.end();
    } catch(error) {
        console.log(error);
        res.status(500).json({message : 'Vous avez une erreur'})
    }
    
});

userRoute.get('/show/all', async (req, res) => {
    try{
         let user = await new UserRepository().FindAll();
         res.json(user)
    
    res.end();
    } catch(error){
        console.log(error);
        res.status(500).json({message: "error error"
        });
    }
   
});

userRoute.get('/show/select/:id', async (req, res) => {
    try{
        let user = await new UserRepository().FindById(req.params.id);
    res.json(user);
    res.end();
    } catch(error){
        console.log(error);
        res.status(500).json({message: 'introuvable'})
    }
    
    
});

userRoute.put('/update', async (req, res)=>{
    
    await new UserRepository().update(req.body);
    res.end();
   
    

})