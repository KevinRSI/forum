import { Router } from "express";
import { MessageRepository } from "../repository/msg_repository.js";

export const msgRoute = Router();

msgRoute.delete('/delete/:id', async (req, res)=>{
    await new MessageRepository().deleteMsg(req.params.id);
    res.end();
})

msgRoute.put('/update', async (req, res)=>{
    await new MessageRepository().updateMsg(req.body);
    res.end();
})

msgRoute.post('/add', async (req, res)=>{
    
    await new MessageRepository().addInMsg(req.body);
    res.end(); 
})