import { Router } from "express";
import { MessageRepository } from "../repository/msg_repository.js";
import { SubjectRepo } from "../repository/subject_repository.js";

export const subRoute = Router();

subRoute.get("/show/all", async (req, res)=>{
    try {
        let data = await new SubjectRepo().findAll();
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})

subRoute.get('/show/select/:id', async (req, res)=>{
    try {
        let data = await new SubjectRepo().findById(req.params.id);
        res.json(data);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})

subRoute.get('/show/:id/msg', async (req, res)=>{
    try {
        let [dataSub] = await new SubjectRepo().findById(req.params.id);
        dataSub.messages = await new MessageRepository().findMsgBySubId(req.params.id);
        res.json(dataSub);
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})

subRoute.post('/add', async (req, res)=>{
    try {        
        await new SubjectRepo().add(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})

subRoute.put('/update', async (req, res)=>{
    try {
        await new SubjectRepo().update(req.body);
        res.end();
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: 'Server Error'
        })
    }
})

subRoute.delete('/delete/:id', async (req, res)=>{
    try {
        await new SubjectRepo().delete(req.params.id);
        res.end();
    } catch (error) {
        res.status(500).json({
            message: 'Server Error'
        })
    }
})