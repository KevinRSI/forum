import { Router } from "express";
import { categorieRepository } from "../repository/categories_repository.js";
import { SubjectRepo } from "../repository/subject_repository.js";

export const catRoute = Router();

catRoute.get('/show/all', async(req, res) => {
    let result = await new categorieRepository().findAll();
    res.json(result);
})

catRoute.get('/show/select/:id', async (req, res) => {
    let data = await new categorieRepository().findById(req.params.id);
    res.json(data);
    res.end();
})

catRoute.put('/update', async (req, res) => {
    await new categorieRepository().update(req.body);
    res.end();
})

catRoute.post('/add', async (req, res) => { 
    await new categorieRepository().add(req.body);
    res.end();
})

catRoute.get('/show/:id/subjects', async (req, res) => {
    let [dataCat] = await new categorieRepository().findById(req.params.id);
    dataCat.subjects = await new SubjectRepo().findByCatId(req.params.id);
    res.json(dataCat);
    res.end();
})

catRoute.delete('/delete/:id', async (req, res) => {
    await new categorieRepository().delete(req.params.id);
    res.end();
})

