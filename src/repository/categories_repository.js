import {
    Categorie
} from "../entity/categories.js";
import {
    connection
} from "./connection.js";





export class categorieRepository {
    /**
     * Find all rows in the cat table
     * @returns 
     */
    async findAll(){
        let [rows] = await connection.execute('SELECT * FROM cat');
        let cat = [];
        for (const row of rows) {
            let instance = new Categorie(row.catId, row.title);
            cat.push(instance);
        }
        return cat
    }
    

    /**
     * Find a row by his id in the cat table
     * @param {String} param 
     * @returns 
     */
    async findById(param) {
        const [rows] = await connection.execute(`SELECT * FROM cat WHERE catId=?`, [param]);
        let catById = [];
        for (const row of rows) {
            let instance = new Categorie(row.catId, row.title)
            catById.push(instance);
        }
        return catById;
    }

    /**
     * add a row in the cat table
     * @param {String} title 
     */
    async add(title) {
        const [rows] = await connection.execute(`INSERT INTO cat (title) VALUES (?)`, [title]);
    };

    /**
     * update a row in the cat table
     * @param {Object} updateCat 
     */
    async update(updateCat) {
        const [rows] = await connection.execute(`UPDATE cat SET title=? WHERE catId=?`, [updateCat.title, updateCat.catId]);
    }

    /**
     * delete a row by his id in the cat table
     * @param {Number} catId 
     */
    async delete(catId) {
        const [rows] = await connection.execute(`DELETE FROM cat WHERE catId=?`, [catId]);
    }


}