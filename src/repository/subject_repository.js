import {
    connection
} from "./connection.js";
import {
    Subject
} from "../entity/subject.js";



export class SubjectRepo {
    /**
     * Find every rows in the subject table
     * @returns 
     */
    async findAll() {
        const [rows] = await connection.execute('SELECT * FROM subject');
        const sub = [];
        for (const row of rows) {
            let instance = new Subject(row.SubjectId, row.UserId, row.text, row.catId);
            sub.push(instance);

        }
        return sub;
    };

    /**
     * Find a row by his id in the subject table
     * @param {*} id 
     * @returns 
     */
    async findById(id) {
        const [rows] = await connection.execute(`SELECT * FROM subject WHERE SubjectId=?`, [id]);
        let subById = [];
        for (const row of rows) {
            let instance = new Subject(row.SubjectId, row.UserId, row.text, row.catId);
            subById.push(instance);
        }
        return subById
    }

    /**
     * find a row by his categorie id in the subject table
     * @param {Number} id 
     * @returns 
     */
    async findByCatId(id) {
        const [rows] = await connection.execute(`SELECT * FROM subject WHERE catId=?`, [id]);
        let subById = [];
        for (const row of rows) {
            let instance = new Subject(row.SubjectId, row.UserId, row.text, row.catId);
            subById.push(instance);
        }
        return subById
    }

    /**
     * add a row in the subject table via an object
     * @param {Object} subject 
     */
    async add(subject) {
        const [rows] = await connection.execute(`INSERT INTO subject (UserId,catId,text) VALUES(?, ?, ?)`, [subject.uId, subject.catId, subject.title]);
    };

    /**
     * update a row in the subject table
     * @param {Object} subject 
     */
    async update(subject) {
        const [rows] = await connection.execute(`UPDATE subject SET UserId=?, catId=?, text=? WHERE SubjectId=?`, [subject.uId, subject.catId, subject.title, subject.subId]);
    };

    /**
     * Delete a row by his id
     * @param {Number} id 
     */
    async delete(id) {
        const [rows] = await connection.execute(`DELETE FROM subject WHERE SubjectId=?`, [id]);
    };

}