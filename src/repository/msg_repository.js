import {
    Message
} from "../entity/msg.js";
import {
    connection
} from "./connection.js";



export class MessageRepository {
    /**
     * Find every rows in the msg table
     * @returns 
     */
    async findAllmsg() {
        const [rows] = await connection.execute('SELECT * FROM msg');
        const messages = [];
        for (const row of rows) {
            let instance = new Message(row.content, row.SubjectId, row.UserId, row.msgId);
            messages.push(instance);
        }
        return messages;
    }

    /**
     * Find a row by his id in the msg table
     * @param {Number} Id 
     * @returns 
     */
    async findmsgById(Id) {
        const [rows] = await connection.execute('SELECT * FROM msg WHERE msgId =?', [Id]);
        const message = [];
        for (const row of rows) {
            let instance = new Message(row.content, row.SubjectId, row.UserId, row.msgId);
            message.push(instance);
        }
        return message;
    }

    /**
     * Find a row by his subject id in the msg table
     * @param {Number} Id 
     * @returns 
     */
    async findMsgBySubId(Id) {
        const [rows] = await connection.execute('SELECT * FROM msg WHERE SubjectId =?', [Id]);
        const message = [];
        for (const row of rows) {
            let instance = new Message(row.content, row.SubjectId, row.UserId, row.msgId);
            message.push(instance);
        }
        return message;
    }

    /**
     * add a row in the msg table
     * @param {Object} msg 
     */
    async addInMsg(msg) {
        await connection.execute(`INSERT INTO msg (content, UserId, SubjectId) VALUES(?, ?, ? )`, [msg.content, msg.UserId, msg.SubId]);

    }

    /**
     * Delete a row in the msg table
     * @param {Number} msgId 
     */
    async deleteMsg(msgId) {
        await connection.execute(`DELETE FROM msg WHERE msgId=?`, [msgId]);
    }

    /**
     * Update a row in the msg table
     * @param {Object} msg 
     */
    async updateMsg(msg) {
        await connection.execute(`UPDATE msg SET content=? WHERE msgId=?`, [msg.content, msg.msgId]);
    }

}