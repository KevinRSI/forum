import {
    User
} from "../entity/user.js";
import {
    connection
} from "./connection.js";


export class UserRepository {
    /**
     * Find every rows in the user table
     * @returns 
     */
    async FindAll() {
        const [rows] = await connection.execute('SELECT * FROM user');
        const users = [];
        for (const row of rows) {
            let instance = new User(row.UserId, row.name, row.pwd, row.mail, row.access)
            users.push(instance);
        }
        return users;

    }

    /**
     * Find a row by his id in the user table
     * @param {Number} param 
     * @returns 
     */
    async FindById(param) {

        const [rows] = await connection.execute(`SELECT * FROM user WHERE UserId=?`, [param])
        const userid = []
        for (const row of rows) {
            let instance = new User(row.UserId, row.name, row.pwd, row.mail, row.access)
            userid.push(instance);
        }
        return userid

    }

    /**
     * Add a row in the user table
     * @param {Object} addUser 
     */
    async Add(addUser) {
        const [rows] = await connection.execute(`INSERT INTO user (name,pwd,mail,access) VALUES (?,?,?,?)`, [addUser.name, addUser.pwd, addUser.mail, addUser.access])

    }

    /**
     * Update a row in the user table
     * @param {Object} user 
     */
    async update(user) {
        const [rows] = await connection.execute(`UPDATE user SET name=?, pwd=?, mail=?, access=? WHERE UserId=?`, [user.name, user.pwd, user.mail, user.access, user.id]);
    }

    /**
     * Delete a row by his id in the user table
     * @param {Number} id 
     */
    async Delete(id) {
        const [rows] = await connection.execute(`DELETE FROM user WHERE UserId=?`, [id])
    }

}