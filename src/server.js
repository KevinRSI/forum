import express from "express";
import { subRoute } from "./routes/subject_routes";
import { catRoute } from "./routes/categories_routes";
import { msgRoute } from "./routes/msg_routes";
import { userRoute } from "./routes/user_routes";
export const server = express();



server.use(express.json());
server.use(express.urlencoded({extended: true}))


server.use('/api/subjects', subRoute);
server.use('/api/category', catRoute);
server.use('/api/messages', msgRoute);
server.use('/api/user', userRoute);

