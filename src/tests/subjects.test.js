import request from "supertest";
import { connection } from "../repository/connection";
import {
    server
} from "../server";

describe('Test API for the subjects route', () => {
    it('should get all subjects and send success', async () => {
        
        let res = await request(server)
            .get('/api/subjects/show/all')
            .expect(200)
            .expect('Content-Type', /json/);
        let index = Math.floor(Math.random(res.body.length))
        expect(res.body[index]).toEqual({
            Text : expect.any(String),
            UserId : expect.any(Number),
            catId : expect.any(Number),
            messages : expect.any(Array),
            subjectId : expect.any(Number)
        })
    })

    it('should get a subject by his id and send success', async()=>{
        let index = Math.floor(Math.random(2)+1)
        let res = await request(server)
        .get(`/api/subjects/show/select/${index}`)
        .expect(200)

        expect(res.body[0]).toEqual({
            Text : expect.any(String),
            UserId : expect.any(Number),
            catId : expect.any(Number),
            messages : [],
            subjectId : expect.any(Number)
        })
    })

    it('should get a subject and all the messages affiliated to his id', async ()=>{
        let index = Math.floor(Math.random(2)+1)
        let res= await request(server)
        .get(`/api/subjects/show/${index}/msg`)
        .expect(200)

        expect(res.body).toEqual({
            Text : expect.any(String),
            UserId : expect.any(Number),
            catId : expect.any(Number),
            messages : expect.any(Array),
            subjectId : expect.any(Number)
        })
    })

    it('should add a subject to the table', async()=>{
        await request(server)
        .post('/api/subjects/add')
        .expect(200)
        .send({
            title : 'try test',
            uId : 2,
            catId : 2
        });

        await connection.execute('DELETE FROM subject WHERE Text="try test" ORDER BY SubjectId desc LIMIT 1')
    })
})