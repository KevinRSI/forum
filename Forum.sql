/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET SQL_NOTES=0 */;


DROP TABLE IF EXISTS cat;
CREATE TABLE `cat` (
  `catId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`catId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS msg;
CREATE TABLE `msg` (
  `msgId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `content` text DEFAULT NULL,
  `UserId` int(11),
  `SubjectId` int(11) NOT NULL,
  PRIMARY KEY (`msgId`),
  KEY `UserId_msg_fk` (`UserId`),
  CONSTRAINT `UserId_msg_fk` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS subject;
CREATE TABLE `subject` (
  `SubjectId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `UserId` int(11),
  `catId` int(11) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`SubjectId`),
  KEY `UserId_fk` (`UserId`),
  KEY `catId_fk` (`catId`),
  CONSTRAINT `UserId_fk` FOREIGN KEY (`UserId`) REFERENCES `user` (`UserId`) ON DELETE SET NULL ON UPDATE SET NULL ,
  CONSTRAINT `catId_fk` FOREIGN KEY (`catId`) REFERENCES `cat` (`catId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS user;
CREATE TABLE `user` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `name` varchar(30) DEFAULT NULL,
  `pwd` varchar(60) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `access` int(11) NOT NULL COMMENT 'level of access',
  PRIMARY KEY (`UserId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO cat(catId,title) VALUES(1,'Grains'),(2,'Cafetières'),(3,'Torréfaction'),(5,'Culture');

INSERT INTO msg(msgId,content,UserId,SubjectId) VALUES(1,X'4a2761696d65206c6520636166c3a92076657274',1,1),(2,X'436166657469c3a87265206974616c69656e6e65206d612064726f677565',3,2),(3,X'4c65206d696575782063276573742061752050c3a9726f75',2,3);

INSERT INTO subject(SubjectId,UserId,catId,text) VALUES(1,1,1,'Quel type de mouture ?'),(2,2,2,'Faut-il une expresso ?'),(3,1,5,'Où pousse l\'arabica ?');
INSERT INTO user(UserId,name,pwd,mail,access) VALUES(1,'toto','1234','toto@coffee.fr',1),(2,'tata','1234','tata@coffee.fr',1),(3,'tutu','1234','tutu@coffee.fr',2);