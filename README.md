# Forum SQL

Au cours de notre formation 'Developpeur Web/ developpeur Mobile' , nous devions, en guise de quatrième projet, concevoir et mettre en place la base de données pour une application de forum.
Ce travail nous l'avons réalisé par groupe de 4 personnes.

Les réalisations étaient les suivantes :

- conception de l'application forum
- créer un script de mise en place de la base de données
- créer un script de mise en place de données de tests
- créer un repository par entité
- créer une application Express
- creér les routes liées aux repository
- créer une API REST
- Deployer sur Heroku

Nous avons opté pour un Forum orienté sur le café.

Nos échanges de données se sont principalement faits via un DRIVE partagé.
Il contient :

- les Uses cases
- les Users stories
- les maquettes fonctionnelles
- la grille d'amélioration
- le diagramme de classes

[ Lien DRIVE = <https://drive.google.com/drive/folders/1bn_rJsm-P9AST31vYRqZph5zfLtwBXC-?usp=sharing> ]

[ Lien Heroku = <https://coffee-forum.herokuapp.com/> ]
